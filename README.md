# Ansible Collection - gorps.infrastructure

This ansible collection consists of basic roles and playbooks.

## Installation

```sh
ansible-galaxy collection install git+https://gitlab.com/g-braeunlich/ansible-infrastructure.git
```

This will clone the project and install it into `~/.ansible/collections/ansible_collections/gorps/infrastructure`.

## Usage

Example: Deploy msmtp:

Create an inventory file for your target host:

File: `hosts.yml`

```yml
all:
  hosts:
    deep.thought:
      remote_user: root
      maintainer_mail: chucknorris@roundhouse.gov
      msmtp_from: chucknorris@roundhouse.gov
      msmtp_host: mail.roundhouse.gov
      msmtp_port: 465
      msmtp_maildomain: roundhouse.gov
      msmtp_user: chucknorris
      msmtp_pass: >-
        Chuck Norris does not need passwords. Mail servers
        grant access to him voluntarily.
```

Then run (need an entry in `.ssh/config` named `deep.thought`):

```sh
ansible-playbook -i hosts.yml gorps.infrastructure.msmtp
```

## Develop


### Installation

Instead of installing the repository using `ansible-galaxy`, you can
clone the repo into the same directory path:

```sh
mkdir -p ~/.ansible/collections/ansible_collections/gorps/
cd ~/.ansible/collections/ansible_collections/gorps/
git clone git@gitlab.com:g-braeunlich/ansible-infrastructure.git
```

Alternatively, clone to some different path, then create or edit
`~/.ansible.cfg` and add the following:

```ini
[defaults]

...
collections_paths = <your custom path>
...
```
